CREATE TABLE users (
    id INT(11) auto_increment NOT NULL,
    civility VARCHAR(10) NOT NULL
    lastname VARCHAR(50) NOT NULL,
    firstname VARCHAR(50) NOT NULL,
    email VARCHAR(75) NOT NULL,
    pwd VARCHAR(75) NOT NULL,
    admin TINYINT(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE contacts (
    id_contact INT(11) auto_increment NOT NULL,
    email VARCHAR(75) NOT NULL,
    message_object VARCHAR(100) NOT NULL,
    message_content TEXT NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_contact),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE appointments (
    id_app INT(11) auto_increment NOT NULL,
    datetime_app DATETIME NOT NULL,
    street_number VARCHAR(25) NULL,
    street_name VARCHAR(75) NULL,
    postal_code VARCHAR(25) NULL,
    city VARCHAR(50) NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_app),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE opinions (
    id_op INT(11) auto_increment NOT NULL,
    opinion_content TEXT NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    art_id INT(11) NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_op),
    FOREIGN KEY(art_id) REFERENCES articles(id_art),
    FOREIGN KEY (user_id) REFERENCES users(id)
);


CREATE TABLE carbone_assessment (
    id_ass INT(11) auto_increment NOT NULL,
    email_number INT(5) NOT NULL,
    attachment_number INT(5) NOT NULL,
    spam_number INT(5) NOT NULL,
    archive_email_number INT(5) NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_ass),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE donation (
    id_don INT(11) auto_increment NOT NULL,
    amount INT(5) NOT NULL,
    holder_name VARCHAR(50) NOT NULL,
    card_type VARCHAR(50) NOT NULL,
    card_number INT(5) NOT NULL,
    cryptogram INT(5) NOT NULL,
    expiration_date DATE NOT NULL,
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_don),
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE articles (
    id_art INT (11) auto_increment NOT NULL,
    title VARCHAR (50) NOT NULL,
    article_content TEXT NOT NULL,
    created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    picture VARCHAR(45),
    user_id INT(11) NOT NULL,
    PRIMARY KEY (id_art),
    FOREIGN KEY (user_id) REFERENCES users(id)

)
