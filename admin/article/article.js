// ************identification de l'admin*********************************//

if (user.admin == 0) window.location.replace("../../index/index.html");


// **********************Tableau des articles********************************//


function createRow(data, insert = false) {
    data.forEach(component => {
        const row = $("<tr></tr>");
        row.attr("id_art", "tr_" + component.id_art)

        const title = $("<td></td>").text(component.title);
        const article_content = $("<td></td>").text(component.article_content);

        const imagectn = $("<td></td>");
        let img = "Article sans image";
        if (component.picture) img = $("<img>").attr("src", "../../admin/article/" + component.picture);
        imagectn.append(img);

        const updatectn = $("<td></td>");
        const a = $("<a></a>");
        a.html("<i class='fa fa-pencil' aria-hidden='true'></i>");
        a.attr("href", "article_update/article_update.html?id=" + component.id_art);
        updatectn.append(a);

        const delctn = $("<td></td>");
        const delbtn = $("<button></button>");
        delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>");

        delbtn.click(() => {
            wantToDelete(component.id_art);
        });
        delctn.append(delbtn);

        row.append(title, article_content, imagectn, updatectn, delctn);
        if (insert) $("tbody").prepend(row);
        else $("tbody").append(row);
    });
}


function insertArticle(title, article_content, picture) {
    const fd = new FormData();
    fd.append('option', "insert");
    fd.append('title', title);
    fd.append('article_content', article_content);
    fd.append('picture', picture);

    $.ajax({
        url: "article.php",
        type: "POST",
        dataType: "json",     
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            createRow([{
                id_art: res.id_art,
                title,
                article_content,
                picture: "pictures/" + res.picture,
            }], true);
        }
    });
}


function wantToDelete(id_art) {
    if (confirm("Etes-vous sûr de vouloir supprimer cet article?")) {
        $.ajax({
            url: "article.php",
            type: "POST",
            dataType: "json",
            data: {
                option: "delete",
                id_art
            },
            success: () => {
                $("#tr_" + id_art).remove();
                window.location.replace("article.html");
            }
        });
    }
}

$.ajax({
    url: "article.php",
    type: "GET",
    dataType: "json",
    data: { 
        option: "select"
    },
    success: (res) => {
        createRow(res.articles);
    }
});

$("form").submit((event) => {
    event.preventDefault();

    const title = $("#title").val();
    const article_content = $("#article_content").val();
    const picture = $('#picture')[0].files[0];

    insertArticle(title, article_content, picture);
    window.location.replace("article.html");
});

$("#searchbar").keyup(() => {
    const search = $("#searchbar").val();

    if (search.length >= 3) {
        $.ajax({
            url: "article.php",
            type: "GET",
            dataType: "json",
            data: {
                option: "search",
                search
            },
            success: (res) => {
                if (res.success) {
                    $("tbody tr").remove();
                    createRow(res.articles);
                } else alert(res.error);
            }
        });
    }
});