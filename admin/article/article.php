<?php

require_once("../../BDD/bac.php");

require("../../function.php");

isConnected();

isAdmin();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;


switch ($method["option"]) {
    case 'select':
        $req = $bac->query("SELECT a.*, CONCAT(firstname, ' ', lastname) as name FROM articles a INNER JOIN users u ON u.id = a.user_id ORDER BY created_at DESC");
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case 'select_id_art':
        if (isset($_GET["id_art"])) {
            $req = $bac->prepare("SELECT * FROM articles WHERE id_art = ?");
            $req->execute([$_GET["id_art"]]);
            $article = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "article" => $article]);

        } else {
    
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;

    case 'insert':
        if (isset($_POST["title"], $_POST["article_content"]) && !empty(trim($_POST["title"])) && !empty(trim($_POST["article_content"]))) {
            $req = $bac->prepare("INSERT INTO articles (title, article_content, user_id, picture) VALUES (:title, :article_content, :user_id, :picture)");
            $req->bindValue(":title", $_POST["title"]);
            $req->bindValue(":article_content", $_POST["article_content"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);

            $picture = false;
            if (isset($_FILES["picture"]["name"])) $picture = picture($_FILES);
            if ($picture) $req->bindValue(":picture", "admin/article/pictures/" . $picture);
            else $req->bindValue(":picture", null);

            $req->execute();

            echo json_encode(["success" => true]);

        } else {

            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion de l'article"]);
        }
        break;

        case 'update':
            if (isset($_POST["title"], $_POST["article_content"], $_POST["id_art"]) && !empty(trim($_POST["title"])) && !empty(trim($_POST["article_content"])) && !empty(trim($_POST["id_art"]))) {
                $imgsql = "";
                if (isset($_FILES["picture"]["name"])) $imgsql = ", picture = :picture";
    
                $req = $bac->prepare("UPDATE articles SET title = :title, article_content = :article_content, picture = :picture $imgsql WHERE id_art = :id_art");
                $req->bindValue(":title", $_POST["title"]);
                $req->bindValue(":article_content", $_POST["article_content"]);
                $req->bindValue(":id_art", $_POST["id_art"]);
    
                $picture = false;
                if (isset($_FILES["picture"]["name"])) $picture = picture($_FILES);
                if ($picture) $req->bindValue(":picture", "admin/article/pictures/" . $picture);
                else $req->bindValue(":picture", null);
    
                $req->execute();

                echo json_encode(["success" => true]);
            } else {

                echo json_encode((["success" => false, "error" => "Erreur lors de la mise à jour"]));
            }
            break;
    
        case 'delete':
            if (isset($_POST["id_art"]) && !empty(trim($_POST["id_art"]))) {
                $req = $bac->prepare("DELETE FROM articles WHERE id_art = ?");
                $req->execute([$_POST["id_art"]]); 
    
                echo json_encode((["success" => true]));
            } else {

                echo json_encode((["success" => false, "error" => "Erreur lors de la suppression"]));
            }
            break;

        case "search":
                if (isset($_GET["search"]) && !empty(trim($_GET["search"]))) {
        
                    $req = $bac->prepare("SELECT id_art, title, article_content, picture FROM articles WHERE title LIKE ? OR article_content LIKE ?");
                    for ($i = 0; $i < 2; $i++) $data[] = "%{$_GET['search']}%";
                    $req->execute($data);
        
                    $articles = $req->fetchAll(PDO::FETCH_ASSOC);
        
                    echo json_encode(["success" => true, "articles" => $articles]);
            } else {
                    echo json_encode(["success" => false, "error" => "Données manquantes"]);
            }
        
            break;
            
    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}

?>
