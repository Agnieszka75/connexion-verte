// ****** On récupère les parametres de URL et l'id à modifier**************//
const urlParams = new URLSearchParams(window.location.search);
const id_art = urlParams.get("id");

// **** Requête AJAX et affectation au champs du formulaire les valeur de l'utilisateur**/
$.ajax({
    url: "../article.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id_art",
        id_art
    },
    success: (res) => {
        if (res.success) {
            $("#name").text(res.article.title);

            $("#title").val(res.article.title);
            $("#article_content").val(res.article.article_content);
        } else alert(res.error);
    }
});
//*Méthode event.preventDefault pour empêcher que la page soit rechargée à la soumission du formulaire recharge la page*//
// **Récupération de données du formulaire et rédirection de la page*********//

$("form").submit(event => {
    event.preventDefault();

    const fd = new FormData();
    fd.append("option", "update");
    fd.append("id_art", id_art);
    fd.append("title", $("#title").val());
    fd.append("article_content", $("#article_content").val());
    fd.append("picture", $('#picture')[0].files[0]);

    $.ajax({
        url: "../article.php",
        type: "POST",
        dataType: "json",
        data: fd,
        contentType: false,
        processData: false,
        cache: false,
        success: (res) => {
            if (res.success) window.location.replace("../article.html");
            else alert(res.error);
        }
    });
});