<?php
// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("../utils/db_connect.php");
// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("../utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();
// J'appelle ma fonction pour savoir si mon utilisateur est admin
isAdmin();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

//? En fonction du paramètre "choice" de ma requête j'execute les instructions de la case correspondante
switch ($method["choice"]) {
    case 'select':
        // Je récupère tous les articles ainsi que leur auteur
        $req = $db->query("SELECT a.*, CONCAT(firstname, ' ', lastname) FROM articles a INNER JOIN users u ON u.id = a.user_id ORDER BY created_at DESC");

        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case 'select_id':
        //? Si j'ai un paramètre "id" dans ma requête GET alors
        if (isset($_GET["id"])) {
            // Je récupère l'article ciblé par l'id en paramètre
            $req = $db->prepare("SELECT * FROM articles WHERE id = ?"); // J'écris une requete préparée
            $req->execute([$_GET["id"]]); // J'execute ma requete

            // J'affecte à ma variable $article le résultat unique (ou pas de résultat) de ma requete SQL
            $article = $req->fetch(PDO::FETCH_ASSOC);

            // J'envoie une réponse avec un success true ainsi que l'article
            echo json_encode(["success" => true, "article" => $article]);
        } else { //? Sinon
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;

    case 'update':
        //? Si j'ai les paramètres "name", "description", "id" et qu'ils sont non vides alors
        if (isset($_POST["name"], $_POST["description"], $_POST["id"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["description"])) && !empty(trim($_POST["id"]))) {
            $imgsql = "";
            if (isset($_FILES["picture"]["name"])) $imgsql = ", image = :img";

            // J'écris une requete préparée de mise à jour de l'article
            $req = $db->prepare("UPDATE articles SET name = :name, description = :description $imgsql WHERE id = :id");
            // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
            $req->bindValue(":name", $_POST["name"]);
            $req->bindValue(":description", $_POST["description"]);
            $req->bindValue(":id", $_POST["id"]);

            $img = false; // Je défini img à false par défaut
            if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload
            if ($img) $req->bindValue(":img", "../assets/article/" . $img); // Je bind le chemin si la réponse de upload() n'est pas false

            $req->execute();

            // J'envoie une réponse avec un success true
            echo json_encode(["success" => true]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode((["success" => false, "error" => "Erreur lors de la mise à jour"]));
        }
        break;

    case 'delete':
        //? Si j'ai un paramètre "id" et qu'il est non vide alors
        if (isset($_POST["id"]) && !empty(trim($_POST["id"]))) {
            // J'écris une requete préparée de suppression de l'article
            $req = $db->prepare("DELETE FROM articles WHERE id = ?");
            $req->execute([$_POST["id"]]); // J'execute ma requete

            // J'envoie une réponse avec un success true
            echo json_encode((["success" => true]));
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode((["success" => false, "error" => "Erreur lors de la suppression"]));
        }
        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
