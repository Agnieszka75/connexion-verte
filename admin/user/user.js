// ******Si l'utilisateur n'est pas admin il est redirigé vers login*********//

if (user.admin == 0) window.location.replace("../../login/login.html");


// ****************Créaction de la liste des utilisateurs******************//

function createRow(data) {
    data.forEach(c => {
        const row = $("<tr></tr>");
        const lastname = $("<td></td>").text(c.lastname);
        const firstname = $("<td></td>").text(c.firstname);
        const email = $("<td></td>").text(c.email);

        const updateuser = $("<td></td>");
        const a = $("<a></a>");
        a.html("<i class='fa fa-pencil' aria-hidden='true'></i>");
        a.attr("href", "user_update/user_update.html?id=" + c.id);
        updateuser.append(a);

        const delctn = $("<td></td>");
        const delbtn = $("<button></button>");
        delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>");

        delbtn.click(() => {
            wantToDelete(c.id);
        });
        delctn.append(delbtn);

        row.append(lastname, firstname, email, updateuser, delctn);
        $("tbody").append(row);
    });
}

// ****** Requête AJAX et Créaction de function pour supprimer l'utilisateur*******//

$.ajax({
    url: "user.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select"
    },
    success: (res) => {
        createRow(res.users);
    }
});

function wantToDelete(id) {
    if (confirm("Etes-vous sûr de vouloir supprimer cet utilisateur?")) {
        $.ajax({
            url: "user.php",
            type: "POST",
            dataType: "json",
            data: {
                option: "delete",
                id
            },
            success: () => {
                $("#tr_" + id).remove();
                window.location.replace("user.html");
            }
        });
    }
}

// ************************** Searchbar***************************//

$("#searchbar").keyup(() => {
    const search = $("#searchbar").val();

    if (search.length >= 3) {
        $.ajax({
            url: "user.php",
            type: "GET",
            dataType: "json",
            data: {
                option: "search",
                search
            },
            success: (res) => {
                if (res.success) {
                    $("tbody tr").remove();
                    createRow(res.users);
                } else alert(res.error);
            }
        });
    }
});
