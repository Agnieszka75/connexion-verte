<?php

require_once("../../BDD/bac.php");

require("../../function.php");

isConnected();

isAdmin();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;

else $method = $_GET;

switch ($method["option"]) {
    case 'select':

        $req = $bac->query("SELECT id, firstname, lastname, email FROM users");
        $users = $req->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "users" => $users]);

        break;

    case 'select_id':
        if (isset($_GET["id"])) {

            $req = $bac->prepare("SELECT id, firstname, lastname, email FROM users WHERE id=?");
            $req->execute([$_GET["id"]]);

            $user = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "user" => $user]);
        } else {
            echo json_encode(["success" => false, "error" => "Utilisateur inconnu."]);
        }

        break;

    case 'update':
        if (isset($_POST["firstname"], $_POST["lastname"], $_POST["email"], $_POST["id"]) && !empty(trim($_POST["firstname"])) && !empty(trim($_POST["lastname"])) && !empty(trim($_POST["email"])) && !empty(trim($_POST["id"]))) {

            $req = $bac->prepare("UPDATE users SET firstname = ?, lastname = ?, email = ? WHERE id = ?");
            $req->execute([$_POST["firstname"], $_POST["lastname"], $_POST["email"], $_POST["id"]]);

            echo json_encode(["success" => true]);
        } else {
        
            echo json_encode(["success" => false, "error" => "Erreur de la mise à jour."]);
        }
        break;

    case 'delete':
        if (isset($_POST["id"]) && !empty(trim($_POST["id"]))) {
            $req = $bac->prepare("DELETE FROM users WHERE id = ?");
            $req->execute([$_POST["id"]]); 
    
            echo json_encode((["success" => true]));
        } else {

                echo json_encode((["success" => false, "error" => "Erreur lors de la suppression de l'utilisateur"]));
        }
        break;

    case "search":
        if (isset($_GET["search"]) && !empty(trim($_GET["search"]))) {

            $req = $bac->prepare("SELECT id, lastname, firstname, email FROM users WHERE lastname LIKE ? OR firstname LIKE ? OR email LIKE ?");
            for ($i = 0; $i < 3; $i++) $data[] = "%{$_GET['search']}%";
            $req->execute($data);

            $users = $req->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "users" => $users]);
        } else {
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
        }

        break;

    default:
    
        echo json_encode(["success" => false, "error" => "Demande inconnue."]);
        break;
}
