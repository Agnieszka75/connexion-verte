// ****** On récupère les parametres de URL et l'id à modifier**************//
const urlParams = new URLSearchParams(window.location.search);
const id = urlParams.get("id");

// **** Requête AJAX et affectation au champs du formulaire les valeur de l'utilisateur**/
$.ajax({
    url: "../user.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id",
        id
    },
    success: (res) => {
        if (res.success) {
            $("#name").text(res.user.firstname + " " + res.user.lastname);

            $("#firstname").val(res.user.firstname);
            $("#lastname").val(res.user.lastname);
            $("#email").val(res.user.email);
        } else alert(res.error);
    }
});

//*Méthode event.preventDefault pour empêcher que la page soit rechargée à la soumission du formulaire recharge la page*//
// **Récupération de données du formulaire et rédirection de la page*********//

$("form").submit(event => {
    event.preventDefault();

    const firstname = $("#firstname").val();
    const lastname = $("#lastname").val();
    const email = $("#email").val();

    $.ajax({
        url: "../user.php",
        type: "POST",
        dataType: "json",
        data: {
            option: "update",
            id,
            firstname,
            lastname,
            email
        },
        success: (res) => {
            if (res.success) window.location.replace("../user.html"); 
            else alert(res.error);
        }
    });
});