<?php 

define("URL", str_replace("index.php","",(isset($_SERVER['HTTPS'])? "https" : "http").
"://".$_SERVER['HTTP_HOST'].$_SERVER["PHP_SELF"]));

function getImpacts(){
    $pdo = getConnexion();
    $req = "SELECT i.id, i.libelle, i.impactkgco2, i.image, c.libelle as 'categorie'from impactcarb i inner join categorie c on i.categorie_id = c.id";
    $stmt = $pdo->prepare($req);
    $stmt->execute();
    $impacts = $stmt->fetchAll(PDO::FETCH_ASSOC);
    for($i=0;$i< count($impacts);$i++){
        $impacts[$i]['image'] = URL."image/".$impacts[$i]['image'];
    }
    $stmt->closeCursor();
    sendJSON($impacts);
}
function getImpactsByCategorie($categorie){
    $pdo = getConnexion();
    $req = "SELECT i.id, i.libelle, i.impactkgco2, i.image, c.libelle as 'categorie' from impactcarb i inner join categorie c on i.categorie_id = c.id where c.libelle = :categorie";
    $stmt = $pdo->prepare($req);
    $stmt->bindValue(":categorie",$categorie,PDO::PARAM_STR);
    $stmt->execute();
    $impacts = $stmt->fetchAll(PDO::FETCH_ASSOC);
    for($i=0;$i< count($impacts);$i++){
        $impacts[$i]['image'] = URL."image/".$impacts[$i]['image'];
    }
    $stmt->closeCursor();
    sendJSON($impacts);
}
function getImpactById($id){
    $pdo = getConnexion();
    $req = "SELECT i.id, i.libelle, i.impactkgco2, i.image, c.libelle as 'categorie'from impactcarb i inner join categorie c on i.categorie_id = c.id where i.id = :id";
    $stmt = $pdo->prepare($req);
    $stmt->bindValue(":id",$id,PDO::PARAM_INT);
    $stmt->execute();
    $impact = $stmt->fetch(PDO::FETCH_ASSOC);
    $impact['image'] = URL."image/".$impact['image'];
    $stmt->closeCursor();
    sendJSON($impact);
}

function getConnexion(){
    return new PDO("mysql:host=localhost;dbname=bac;charset=utf8","root","");
}

function sendJSON($infos){
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json");
    echo json_encode($infos,JSON_UNESCAPED_UNICODE);
}