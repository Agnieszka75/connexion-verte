<?php
require_once("../../BDD/bac.php");
require_once("./api.php");

try{
    if(!empty($_GET['demande'])){
        $url = explode("/", filter_var($_GET['demande'],FILTER_SANITIZE_URL));
        switch($url[0]){
            case "impacts" : 
                if(empty($url[1])){
                    getImpacts();
                } else {
                    getImpactsByCategorie($url[1]);
                }
            break;
            case "impact" : 
                if(!empty($url[1])){
                    getImpactById($url[1]);
                } else {
                    throw new Exception ("Vous n'avez pas renseigné le numéro d'impact");
                }
            break;
            default : throw new Exception ("demande invalide, controler l'url");
        }
    } else {
        throw new Exception ("Nous ne parvenons pas à récupérer les données.");
    }
} catch(Exception $e){
    $erreur =[
        "message" => $e->getMessage(),
        "code" => $e->getCode()
    ];
    print_r($erreur);
}
