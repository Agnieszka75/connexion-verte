<?php
$impacts = json_decode(file_get_contents("http://localhost/ecoit/api_impact/API/impacts"));
ob_start();
?>

<table class="table">
    <tr>
        <td>Id</td>
        <td>Nom</td>
        <td>impactkgco2</td>
        <td>Image</td>
        <td>Categorie</td>
    </tr>
    <?php foreach ($impacts as $impact) : ?>
        <tr>
            <td><?= $impact->id ?></td>
            <td><a href="impact.php?numero=<?= $impact->id ?>"><?= $impact->libelle ?></a></td>
            <td><?= $impact->impactkgco2 ?></td>
            <td><img src="<?= $impact->image ?>" width="100px;" /></td>
            <td><a href="impactsCategorie.php?categorie=<?= $impact->categorie ?>"><?= $impact->categorie ?></a></td>
        </tr>
    <?php endforeach; ?>
</table>


<?php
$content = ob_get_clean();
require_once("template.php");