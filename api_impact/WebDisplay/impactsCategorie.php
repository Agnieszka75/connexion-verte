<?php
$impacts = json_decode(file_get_contents("http://localhost/ecoit/api_impact/API/impacts/".$_GET['categorie']));
ob_start();
?>
<h1>Les impacts de la catégorie <?= $_GET['categorie']; ?></h1>
<table class="table">
    <tr>
        <td>Id</td>
        <td>Nom</td>
        <td>impactkgco2</td>
        <td>Image</td>
        <td>Categorie</td>
    </tr>
    <?php foreach ($impacts as $impact) : ?>
        <tr>
            <td><?= $impact->id ?></td>
            <td><?= $impact->libelle ?></td>
            <td><?= $impact->impactkgco2 ?></td>
            <td><img src="<?= $impact->image ?>" width="100px;" /></td>
            <td><?= $impact->categorie ?></td>
        </tr>
    <?php endforeach; ?>
</table>


<?php
$content = ob_get_clean();
require_once("template.php");