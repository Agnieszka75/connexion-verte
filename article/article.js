// **********************Tableau des articles********************************//


function createRow(data) {
    data.forEach(component => {
        const row = $("<tr></tr>");
        row.attr("id_art", "tr_" + component.id_art)

        const title = $("<td></td>").text(component.title);
        const article_content = $("<td></td>").text(component.article_content);

        const imagectn = $("<td></td>");
        let img = "Article sans image";
        if (component.picture) img = $("<img>").attr("src", "../admin/article/" + component.picture);
        imagectn.append(img);

        const advice = $("<td></td>");
        const a = $("<a></a>");
        a.html("<i class='fa fa-pencil' aria-hidden='true'></i>");
        a.attr("href", "../opinion/opinion.html?id=" + component.id_art);
        advice.append(a);

        row.append(title, article_content, imagectn, advice);
        $("tbody").append(row);
    });
}

$.ajax({
    url: "article.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select"
    },
    success: (res) => {
        createRow(res.articles);
    }
});

// ***********************************Searchbar/ Recherche min 3 caractères***********************************//

$("#searchbar").keyup(() => {
    const search = $("#searchbar").val();

    if (search.length >= 3) {
        $.ajax({
            url: "article.php",
            type: "GET",
            dataType: "json",
            data: {
                option: "search",
                search
            },
            success: (res) => {
                if (res.success) {
                    $("tbody tr").remove();
                    createRow(res.articles);
                } else alert(res.error);
            }
        });
    }
});