<?php
// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("utils/db_connect.php");
// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

//? En fonction du paramètre "choice" de ma requête j'execute les instructions de la case correspondante
switch ($method["choice"]) {
    case 'select':
        // Je récupère tous les articles ainsi que leur auteur
        $req = $db->prepare("SELECT a.*, CONCAT(firstname, ' ', lastname) AS author, IF(uhf.user_id IS NOT NULL, true, false) AS fav FROM articles a INNER JOIN users u ON u.id = a.user_id LEFT JOIN users_has_favorites uhf ON uhf.article_id = a.id AND uhf.user_id = ? ORDER BY created_at DESC;");
        $req->execute([$_SESSION["user_id"]]);

        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case 'select_id':
        //? Si j'ai un paramètre "id" dans ma requête GET alors
        if (isset($_GET["id"])) {
            // Je récupère l'article ciblé par l'id en paramètre et si j'en suis l'auteur
            $req = $db->prepare("SELECT * FROM articles WHERE id=? AND user_id=?"); // J'écris une requete préparée
            $req->execute([$_GET["id"], $_SESSION["user_id"]]); // J'execute ma requete

            // J'affecte à ma variable $article le résultat unique (ou pas de résultat) de ma requete SQL
            $article = $req->fetch(PDO::FETCH_ASSOC);

            // J'envoie une réponse avec un success true ainsi que l'article
            echo json_encode(["success" => true, "article" => $article]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection"]);
        }
        break;

    case 'select_onlyfav':
        // Je récupère tous les articles ainsi que leur auteur
        $req = $db->prepare("SELECT a.*, CONCAT(firstname, ' ', lastname) AS author, 1 AS fav FROM articles a INNER JOIN users u ON u.id = a.user_id LEFT JOIN users_has_favorites uhf ON uhf.article_id = a.id WHERE uhf.user_id = ? ORDER BY created_at DESC;");
        $req->execute([$_SESSION["user_id"]]);

        // J'affecte la totalité de mes résultats à la variable $articles
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les articles
        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case 'insert':
        //? Si j'ai les paramètres "name", "description" et qu'ils sont non vides alors
        if (isset($_POST["name"], $_POST["description"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["description"]))) {
            // J'écris une requete préparée d'insertion de mes données dans la table article
            $req = $db->prepare("INSERT INTO articles (name, description, user_id, image) VALUES (:name, :description, :user_id, :img)");
            // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
            $req->bindValue(":name", $_POST["name"]);
            $req->bindValue(":description", $_POST["description"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);

            $img = false; // Je défini img à false par défaut
            if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload
            if ($img) $req->bindValue(":img", "../assets/article/" . $img); // Je bind le chemin si la réponse de upload() n'est pas false
            else $req->bindValue(":img", null); // Je bind null sinon

            $req->execute(); // J'execute ma requete

            // J'envoie une réponse avec un success true
            echo json_encode(["success" => true, "id" => $db->lastInsertId(), "image" => $img]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    case 'update':
        //? Si j'ai les paramètres "name", "description", "id" et qu'ils sont non vides alors
        if (isset($_POST["name"], $_POST["description"], $_POST["id"]) && !empty(trim($_POST["name"])) && !empty(trim($_POST["description"])) && !empty(trim($_POST["id"]))) {
            $imgsql = "";
            if (isset($_FILES["picture"]["name"])) $imgsql = ", image = :img";

            // J'écris une requete préparée de mise à jour de l'article si j'en suis l'auteur
            $req = $db->prepare("UPDATE articles SET name = :name, description = :description $imgsql WHERE id = :id AND user_id = :user_id");
            // J'affecte à chaque clé les valeurs correspondantes grâce au bindValue
            $req->bindValue(":name", $_POST["name"]);
            $req->bindValue(":description", $_POST["description"]);
            $req->bindValue(":id", $_POST["id"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);

            $img = false; // Je défini img à false par défaut
            if (isset($_FILES["picture"]["name"])) $img = upload($_FILES); // Je récupère la réponse de l'upload
            if ($img) $req->bindValue(":img", "../assets/article/" . $img); // Je bind le chemin si la réponse de upload() n'est pas false

            $req->execute(); // J'execute ma requete
            // J'envoie une réponse avec un success true
            echo json_encode(["success" => true]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Erreur de mise à jour"]);
            die;
        }

        break;

    case 'delete':
        //? Si j'ai un paramètre "id" et qu'il est non vide alors
        if (isset($_POST["id"]) && !empty(trim($_POST["id"]))) {
            // J'écris une requete préparée de suppression de l'article si j'en suis l'auteur
            $req = $db->prepare("DELETE FROM articles WHERE id=? AND user_id=?");
            $req->execute([$_POST["id"], $_SESSION["user_id"]]); // J'execute ma requete

            // J'envoie une réponse avec un success true
            echo json_encode(["success" => true]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Erreur de suppresssion"]);
        }
        break;

    case 'favorite':
        //? Si j'ai un paramètre "article_id" et qu'il est non vide alors
        if (isset($_POST["article_id"]) && !empty(trim($_POST["article_id"]))) {
            if ($_POST["favorite"] == 0) $sql = "INSERT INTO users_has_favorites(user_id, article_id) VALUES (?, ?)";
            else $sql = "DELETE FROM users_has_favorites WHERE user_id = ? AND article_id = ?";

            // J'écris une requete préparée d'ajout ou de suppression de favoris sur l'article
            $req = $db->prepare($sql);
            $req->execute([$_SESSION["user_id"], $_POST["article_id"]]); // J'execute ma requete

            // J'envoie une réponse avec un success true
            echo json_encode(["success" => true]);
        } else {
            // J'envoie une réponse avec un success false et un message d'erreur
            echo json_encode(["success" => false, "error" => "Données manquantes"]);
        }
        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
