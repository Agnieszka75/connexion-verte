<?php

require_once("../BDD/bac.php");

require("../function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["option"]) {
    case 'select':
        $req = $bac->query("SELECT c.*, CONCAT(firstname, ' ', lastname) FROM contacts c INNER JOIN users u ON u.id = c.user_id");
        $contact = $req->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "contacts" => $contact]);
        break;

    case 'select_id':
        if (isset($_GET["id_contact"])) {
            $req = $bac->prepare("SELECT * FROM contacts WHERE id_contact=? AND user_id=?");
            $req->execute([$_GET["id_contact"], $_SESSION["user_id"]]);
            $contact = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "contacts" => $contact]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection"]);
        }
        break;

    case 'insert':
        if (isset($_POST["email"],$_POST["message_object"],$_POST["message_content"]) && !empty(trim($_POST["email"]))&& !empty(trim($_POST["message_object"])) && !empty(trim($_POST["message_content"]))) {
            $req = $bac->prepare("INSERT INTO contacts (email, message_object, message_content, user_id) VALUES (:email, :message_object, :message_content, :user_id)");
            $req->bindValue(":email", $_POST["email"]);
            $req->bindValue(":message_object", $_POST["message_object"]);
            $req->bindValue(":message_content", $_POST["message_content"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    case 'update':
        if (isset($_POST["email"], $_POST["message_object"], $_POST["message_content"], $_POST["id_contact"]) && !empty(trim($_POST["email"])) && !empty(trim($_POST["message_object"]))&& !empty(trim($_POST["message_content"])) && !empty(trim($_POST["id_contact"]))) {
            $req = $bac->prepare("UPDATE contacts SET email = :email, message_object = :message_object, message_content = :message_content, id_contact = :id_contact WHERE id_contact = :id_contact AND user_id = :user_id");
            $req->bindValue(":email", $_POST["email"]);
            $req->bindValue(":message_object", $_POST["message_object"]);
            $req->bindValue(":message_content", $_POST["message_content"]);
            $req->bindValue(":id_contact", $_POST["id_contact"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "erreur de MAJ"]);
        }
        break;

    case 'delete':
        if (isset($_POST["id_contact"]) && !empty(trim($_POST["id_contact"]))) {
            $req = $bac->prepare("DELETE FROM contacts WHERE id_contact=? AND user_id=?");
            $req->execute([$_POST["id_contact"], $_SESSION["user_id"]]);

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "Ereur de suppresssion"]);
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
