<?php

session_start();

function isAdmin()
{
    if (!isset($_SESSION["admin"]) || !$_SESSION["admin"]) {
        echo json_encode(["success" => false, "error" => "Autorisation refusee."]);
        die;
    }
}


function isConnected()
{
    if (!isset($_SESSION["connected"]) || !$_SESSION["connected"]) {
        echo json_encode(["success" => false, "error" => "Defaut de connexion."]);
        die;
    }
}

function picture($file)
{
    $name = $file["picture"]["name"];

    $location = __DIR__ . "/admin/article/pictures/$name";

    $extension = pathinfo($location, PATHINFO_EXTENSION);
    $extension = strtolower($extension);

    $valid_extensions = ["jpg", "jpeg", "png"];

    if (in_array($extension, $valid_extensions)) {
        
        if (move_uploaded_file($file["picture"]["tmp_name"], $location)) return $name;
        else return false;
    } else return false;

}
?>
