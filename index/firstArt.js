// Rechercher dans l'url 
const urlParams = new URLSearchParams(window.location.search);

// Je stock dans id_art, "id" present dans l'url
const id_art = urlParams.get("id");


// Ajax pour sélectionner les infos de l'article en fonction de son ID 
$.ajax({
    url: "firstArt.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id_art",
        id_art
    },
    success: (res) => {
        if (res.success) {

            // On affiche les infos de l'article 
            const article = $("<article></article>");
            article.attr("id" + res.article.id_art);

            const title_adv = $("<h2></h2>").text(res.article.title);

            const article_content = $("<p></p>").text(res.article.article_content);

            article.append(title_adv, article_content);
            $("#advice").append(article);

        } else alert(res.error);
    }
});


// Ajax selectionne les opinions en fonction de l'article 
$.ajax({
    url: "firstArt.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id_op",
        art_id: id_art,
    },

    success: (res) => {
        if (res.success) {
            res.opinions.forEach(component => {

                const row = $("<tr></tr>");
                row.attr("id_op", "tr_" + component.id_op)

                const title = $("<td></td>").text(component.title);
                const article_content = $("<td></td>").text(component.article_content);
                const advice = $("<td></td>").text(component.opinion_content);
                const author = $("<td></td>").text(component.name);
                
                row.append(title, article_content, advice, author);

                $("tbody").append(row);
            })
        }
    }
})
