<?php

require_once("../BDD/bac.php");

require("../function.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["option"]) {

    case 'select_id_art':
        if (isset($_GET["id_art"])) {
            $req = $bac->prepare("SELECT * FROM articles WHERE id_art = ?");
            $req->execute([$_GET["id_art"]]);
            $article = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "article" => $article]);

        } else {
    
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;
        
    case 'select_id_op':
        if (isset($_GET["art_id"])) {
            $req = $bac->prepare("SELECT o.*
            FROM opinions o
            INNER JOIN articles a ON a.id_art = o.art_id
            WHERE (o.art_id = ?)ORDER BY created_at DESC");
            $req->execute([$_GET["art_id"]]);
            $opinion = $req->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "opinions" => $opinion]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la selection"]);
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
