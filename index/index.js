// **********************Tableau des articles********************************//


function createRow(data) {
    data.forEach(component => {
        
        const row = $("<tr></tr>");
        row.addClass("row gy-2 align-items-center");
        row.attr("id_art", "tr_" + component.id_art);
        
        const t = $("<div></div>");
        t.addClass("col-12 col-md-6");

        const title = $("<h2></h2>").text(component.title);
        title.addClass("fw-bold");      
        
        const article_content = $("<td></td>").text(component.article_content);
        article_content.addClass("firstArt");

        const avis = $("<a></a>").attr("href", "firstArt.html?id=" + component.id_art);
        avis.html("<a>Savoir plus</a>");
        avis.addClass("btn btn-dark mt-2 mb-5");

        const t2 = $("<div></div>");
        t2.addClass("col-12 col-md-6");

        const imagectn = $("<img>").attr("src", "../admin/article/" + component.picture);
        // const imagectn = $("<td></td>");
        // let img = "Article sans image";
        // if (component.picture) img = $("<img>").attr("src", "../" + component.picture);
        // imagectn.append(img);
        
        imagectn.prop('width', '635');
        imagectn.prop('height', '240');
        imagectn.addClass("rounded-3 shadow mb-5");
        
        row.append(t, t2);
        t.append(title, article_content, avis);
        t2.append(imagectn);
        $("tbody").append(row);


        // const avis = $("<a></a>");
        // avis.html("<a>Savoir plus</a>");
        // avis.addClass("btn btn-dark mt-5");
        // avis.attr("href", "firstArt.html?id=" + component.id_art);
        // avis.append(avis);
        // $("tbody").append(avis);
    });
}

$.ajax({
    url: "index.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select"
    },
    success: (res) => {
        createRow(res.articles);
    }
});

// ***********************************Searchbar/ Recherche min 3 caractères***********************************//

$("#searchbar").keyup(() => {
    const search = $("#searchbar").val();

    if (search.length >= 3) {
        $.ajax({
            url: "index.php",
            type: "GET",
            dataType: "json",
            data: {
                option: "search",
                search
            },
            success: (res) => {
                if (res.success) {
                    $("tbody tr").remove();
                    createRow(res.articles);
                } else alert(res.error);
            }
        });
    }
});
