<?php

require_once("../BDD/bac.php");

require("../function.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;


switch ($method["option"]) {
    case 'select':
        $req = $bac->query("SELECT a.* FROM articles a ORDER BY created_at DESC");
        // faire LIMIT ?
        $articles = $req->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "articles" => $articles]);
        break;

    case 'select_id_art':
        if (isset($_GET["id_art"])) {
            $req = $bac->prepare("SELECT * FROM articles WHERE id_art = ?");
            $req->execute([$_GET["id_art"]]);
            $article = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "article" => $article]);

        } else {
    
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;

    case "search":
            if (isset($_GET["search"]) && !empty(trim($_GET["search"]))) {
    
                $req = $bac->prepare("SELECT id_art, title, article_content, picture FROM articles WHERE title LIKE ? OR article_content LIKE ?");
                for ($i = 0; $i < 2; $i++) $data[] = "%{$_GET['search']}%";
                $req->execute($data);
    
                $articles = $req->fetchAll(PDO::FETCH_ASSOC);
    
                echo json_encode(["success" => true, "articles" => $articles]);
        } else {
                echo json_encode(["success" => false, "error" => "Données manquantes"]);
        }
    
        break;



    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}

?>