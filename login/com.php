<?php
// Je démarre ma session
session_start();

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("utils/db_connect.php");

//? Si la méthode de ma requête est différente de POST alors
if ($_SERVER["REQUEST_METHOD"] != "POST") {
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Mauvaise méthode"]);
    die; //! J'arrête l'exécution du script
}

//? Si je n'ai pas les paramètres "email" et "password" alors
if (!isset($_POST["email"], $_POST["password"])) {
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Données manquantes"]);
    die; //! J'arrête l'exécution du script
}

//? Si les paramètres "email" et "password" sont vides alors
if (empty(trim($_POST["email"])) || empty(trim($_POST["password"]))) {
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Données vides"]);
    die; //! J'arrête l'exécution du script
}

// J'écris ma requête préparée pour séléctionner l'id, le mot de passe et si l'utilisateur est admin
$req = $db->prepare("SELECT * FROM users WHERE email = ?");
$req->execute([$_POST["email"]]); // J'execute la requête

// J'affecte à ma variable $user le résultat unique (ou pas de résultat) de ma requete SQL
$user = $req->fetch(PDO::FETCH_ASSOC);

//? Si ma variable $user à une valeur ET que le mot de passe correspond au hash de celui de l'utilisateur alors
if ($user && password_verify($_POST["password"], $user["pwd"])) {
    $_SESSION["connected"] = true; // J'affecte à la clé "connected" la valeur true
    $_SESSION["user_id"] = $user["id"]; // J'affecte à la clé "user_id" la valeur de l'id de l'utilisateur qui vient de se connecter
    $_SESSION["admin"] = $user["admin"]; // J'affecte à la clé "admin" la valeur admin de l'utilisateur

    unset($user["pwd"]);

    // J'envoie une réponse avec un success true
    echo json_encode(["success" => true, "user" => $user]);
} else { //? Sinon
    // Je vide ma session
    $_SESSION = [];
    // J'envoie une réponse avec un success false et un message d'erreur
    echo json_encode(["success" => false, "error" => "Utilisateur introuvable"]);
}
