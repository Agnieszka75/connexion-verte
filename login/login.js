const urlParams = new URLSearchParams(window.location.search);

if (urlParams.get("logout")) {

    $.ajax({
        url: "logout.php",
        type: "GET", 
        dataType: "json", 
        success: () => {
            localStorage.removeItem("user");
        }
    });
}

$("form").submit((event) => {
    event.preventDefault();

    $.ajax({
        url: "login.php", 
        type: "POST", 
        dataType: "json", 
        data: { 
            email: $("#email").val(),
            password: $("#password").val()
        },
        success: (res) => {
            if (res.success) {
                localStorage.setItem("user", JSON.stringify(res.user));
                window.location.replace("../user/user.html"); 
            } else alert(res.error); 
        }
    });
});