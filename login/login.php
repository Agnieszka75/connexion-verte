<?php

session_start();

require_once("../BDD/bac.php");

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    echo json_encode(["success" => false, "error" => "Méthode non valide."]);
    die;
}

if (!isset($_POST["email"], $_POST["password"])) {
    echo json_encode(["success" => false, "error" => "Attention! Donnees manquantes."]);
    die;
}

if (empty(trim($_POST["email"])) || empty(trim($_POST["password"]))) {
    echo json_encode(["success" => false, "error" => "Attention! Donnees vides."]);
    die;
}

$req = $bac->prepare("SELECT * FROM users WHERE email = ?");
$req->execute([$_POST["email"]]);
$user = $req->fetch(PDO::FETCH_ASSOC);

if ($user && password_verify($_POST["password"], $user["pwd"])) {
    $_SESSION["connected"] = true;
    $_SESSION["user_id"] = $user["id"];
    $_SESSION["admin"] = $user["admin"];

    unset($user["pwd"]);

    echo json_encode(["success" => true, "user" => $user]);
} else {
    $_SESSION = [];
    echo json_encode(["success" => false, "error" => "Utilisateur introuvable."]);
}
?>
