const urlParams = new URLSearchParams(window.location.search);
const id_art = urlParams.get("id");

function displayArticle(article) {
    const articleContainer = $("<article></article>");
    articleContainer.attr("id", article.id_art);

    const title = $("<h2></h2>").text(article.title);
    const articleContent = $("<p></p>").text(article.article_content);

    articleContainer.append(title, articleContent);
    $("#advice").append(articleContainer);
}

function displayOpinions(opinions) {
    opinions.forEach(opinion => {
        const row = $("<tr></tr>");
        row.attr("id_op", "tr_" + opinion.id_op);

        const title = $("<td></td>").text(opinion.title);
        const articleContent = $("<td></td>").text(opinion.article_content);
        const advice = $("<td></td>").text(opinion.opinion_content);
        const author = $("<td></td>").text(opinion.name);

        row.append(title, articleContent, advice, author);
        $("tbody").append(row);
    });
}

function select_op(id) {
    $.ajax({
        url: "opinion.php",
        type: "GET",
        dataType: "json",
        data: {
            option: "select_id_op_one",
            art_id: id_art,
            id_op: id
        },
        success: (res) => {
            if (res.success) {
                const opinion = res.opinion;
                const row = $("<tr></tr>");
                row.attr("id_op", "tr_" + id);

                const title = $("<td></td>").text(opinion.title);
                const articleContent = $("<td></td>").text(opinion.article_content);
                const advice = $("<td></td>").text(opinion.opinion_content);
                const author = $("<td></td>").text(opinion.name);

                row.append(title, articleContent, advice, author);
                $("tbody").append(row);
            }
        }
    });
}

function insertOpinion(opinion_content) {
    $.ajax({
        url: "opinion.php",
        type: "POST",
        dataType: "json",
        data: {
            option: "insert",
            opinion_content: opinion_content,
            art_id: id_art
        },
        success: (res) => {
            select_op(res.id_op_last);
        }
    });
}

$("form").submit((event) => {
    event.preventDefault();
    const opinionContent = $("#opinion_content").val();
    insertOpinion(opinionContent);
    $("form")[0].reset();
});

// AJAX pour sélectionner les infos de l'article en fonction de son ID
$.ajax({
    url: "opinion.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id_art",
        id_art
    },
    success: (res) => {
        if (res.success) {
            displayArticle(res.article);
        } else {
            alert(res.error);
        }
    }
});

// AJAX pour sélectionner les opinions en fonction de l'article
$.ajax({
    url: "opinion.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_id_op",
        art_id: id_art,
    },
    success: (res) => {
        if (res.success) {
            displayOpinions(res.opinions);
        }
    }
});
