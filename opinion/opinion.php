<?php

require_once("../BDD/bac.php");

require("../function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["option"]) {

    case 'select_id_art':
        if (isset($_GET["id_art"])) {
            $req = $bac->prepare("SELECT * FROM articles WHERE id_art = ?");
            $req->execute([$_GET["id_art"]]);
            $article = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "article" => $article]);

        } else {
    
            echo json_encode(["success" => false, "error" => "Erreur lors de la sélection de l'article"]);
        }
        break;
        
    case 'select':
        $req = $bac->query("SELECT o.*, a.title, CONCAT(firstname, ' ', lastname) AS author FROM opinions o
        INNER JOIN users u ON u.id = o.user_id
        INNER JOIN articles a ON a.id_art = o.art_id");
        
        $opinion = $req->fetchAll(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "opinion" => $opinion]);
        
        break;

    case 'select_id_op':
        if (isset($_GET["art_id"])) {
            $req = $bac->prepare("SELECT o.*, CONCAT(u.firstname, ' ', u.lastname) AS name, title, article_content
            FROM opinions o
            INNER JOIN users u ON u.id = o.user_id
            INNER JOIN articles a ON a.id_art = o.art_id
            WHERE (o.art_id = ?)ORDER BY created_at DESC");
            $req->execute([$_GET["art_id"]]);
            $opinion = $req->fetchAll(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "opinions" => $opinion]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la selection"]);
        }
        break;

        case 'select_id_op_one':

            if (isset($_GET["art_id"])) {
                $req = $bac->prepare("SELECT o.*, CONCAT(u.firstname, ' ', u.lastname) AS name, title, article_content
                FROM opinions o
                INNER JOIN users u ON u.id = o.user_id
                INNER JOIN articles a ON a.id_art = o.art_id
                WHERE (o.art_id = ? AND id_op = ?)");
                $req->execute([$_GET["art_id"], $_GET["id_op"]]);
                $opinion = $req->fetch(PDO::FETCH_ASSOC);
    
                echo json_encode(["success" => true, "opinion" => $opinion]);
            } else {
                echo json_encode(["success" => false, "error" => "Erreur lors de la selection"]);
            }
            break;

    case 'insert':
        if (isset($_POST["opinion_content"]) && !empty(trim($_POST["opinion_content"]))) {

            $req = $bac->prepare("INSERT INTO opinions (opinion_content, art_id, user_id) VALUES (:opinion_content, :art_id, :user_id)");
            $req->bindValue(":opinion_content", $_POST["opinion_content"]);
            // permet le test sur Postman
            $req->bindValue(":art_id", $_POST["art_id"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);
            // permet le fonctionnement de l'option sur JS
            // $req->bindValue(":art_id", $_SESSION["art_id"]);
            $req->execute();

            echo json_encode(["success" => true, "id_op_last" => $bac->lastInsertId()]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
