<?php

require_once("../BDD/bac.php");

require("../function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["option"]) {
    case 'select':
        $req = $bac->query("SELECT o.*, CONCAT(firstname, ' ', lastname) FROM opinions o INNER JOIN users u ON u.id = o.user_id");
        $opinion = $req->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode(["success" => true, "opinions" => $opinion]);
        break;

    case 'select_id':
        if (isset($_GET["id_op"])) {
            $req = $bac->prepare("SELECT * FROM opinions WHERE id_op=? AND user_id=?");
            $req->execute([$_GET["id_op"], $_SESSION["user_id"]]);
            $opinion = $req->fetch(PDO::FETCH_ASSOC);

            echo json_encode(["success" => true, "opinions" => $opinion]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de la selection"]);
        }
        break;

    case 'insert':
        if (isset($_POST["article_title"],$_POST["opinion_content"]) && !empty(trim($_POST["article_title"])) && !empty(trim($_POST["opinion_content"]))) {
            $req = $bac->prepare("INSERT INTO opinions (article_title, opinion_content, user_id) VALUES (:article_title, :opinion_content, :user_id)");
            $req->bindValue(":article_title", $_POST["article_title"]);
            $req->bindValue(":opinion_content", $_POST["opinion_content"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur lors de l'insertion"]);
        }
        break;

    case 'update':
        if (isset($_POST["article_title"], $_POST["opinion_content"], $_POST["id_op"]) && !empty(trim($_POST["article_title"])) && !empty(trim($_POST["opinion_content"])) && !empty(trim($_POST["id_op"]))) {
            $req = $bac->prepare("UPDATE opinions SET article_title = :article_title, opinion_content = :opinion_content, id_op = :id_op WHERE id_op = :id_op AND user_id = :user_id");
            $req->bindValue(":article_title", $_POST["article_title"]);
            $req->bindValue(":opinion_content", $_POST["opinion_content"]);
            $req->bindValue(":id_op", $_POST["id_op"]);
            $req->bindValue(":user_id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "erreur de mise à jour"]);
        }
        break;

    case 'delete':
        if (isset($_POST["id_op"]) && !empty(trim($_POST["id_op"]))) {
            $req = $bac->prepare("DELETE FROM opinions WHERE id_op=? AND user_id=?");
            $req->execute([$_POST["id_op"], $_SESSION["user_id"]]);

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "Ereur de suppresssion"]);
        }
        break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}
