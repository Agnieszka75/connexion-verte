<?php

require_once("../BDD/bac.php");

require("../mailer.php");

if ($_SERVER["REQUEST_METHOD"] != "POST") {
    echo json_encode(["success" => false, "error" => "Méthode non valide"]);
    die;

}

if (!isset($_POST["firstname"], $_POST["lastname"], $_POST["email"], $_POST["password"])) {
    echo json_encode(["success" => false, "error" => "Attention! Données manquantes"]);
    die;

}

if (
    empty(trim($_POST["firstname"])) ||
    empty(trim($_POST["lastname"])) ||
    empty(trim($_POST["email"])) ||
    empty(trim($_POST["password"]))) {
    echo json_encode(["success" => false, "error" => "Attention! Données vides"]);
    die;
}

$regex = "/^[a-zA-Z0-9-+._]+@[a-zA-Z0-9-]{2,}\.[a-zA-Z]{2,}$/";
if (!preg_match($regex, $_POST["email"])) {
    echo json_encode(["success" => false, "error" => "Format email incorrect"]);
    die;
}

$regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
if (!preg_match($regex, $_POST["password"])) {
    echo json_encode(["success" => false, "error" => "Format mot de passe incorrect"]);
    die;
}

$hash = password_hash($_POST["password"], PASSWORD_DEFAULT);

$req = $bac->prepare("INSERT INTO users (firstname, lastname, email, pwd) VALUES (:firstname, :lastname, :email, :pwd)");
$req->bindValue(":firstname", $_POST["firstname"]);
$req->bindValue(":lastname", $_POST["lastname"]);
$req->bindValue(":email", $_POST["email"]);
$req->bindValue(":pwd", $hash);
$req->execute(); 

mailer($_POST["email"], "Bienvenue {$_POST["firstname"]} {$_POST["lastname"]}", "Bienvenue à la Connexion Verte");

echo json_encode(["success" => true]);

?>
