// ******recuperation de donnees et identification user/admin**********//

let user = JSON.parse(localStorage.getItem("user"));

if (!user) {
    window.location.replace("/login/login.html");
}

if(user.admin == 0) {
    $("#admin_menu").remove();
}

$.ajax({
    url: "user.php",
    type: "GET", 
    dataType: "json",
    data: {
        option: "select_id",
    },

    success: (res) => {
        if (res.success) {
            
            $("#firstname").val(res.user.firstname);
            $("#lastname").val(res.user.lastname);
            $("#email").val(res.user.email);
            $("#password").val(res.user.pwd);

            $("#name").text(res.user.firstname + " " + res.user.lastname);//J'affiche le nom de l'utilisateur

        } else alert(res.error);
    }
})

$("form").submit(event => {
    event.preventDefault();

    const firstname = $("#firstname").val();
    const lastname = $("#lastname").val();
    const email = $("#email").val();
    const password = $("#password").val();

    $.ajax({
        url: "user.php",
        type: "POST",
        dataType: "json",
        data: {
            option: "update",
            firstname,
            lastname,
            email,
            password // On ne peut pas mettre à jour le password?
        },
        success: (res) => {
            if (res.success) {

                $("#name").text(firstname + " " + lastname); // J'affiche le nom de l'utilisateur MAJ
                
                user = {
                    id: user.id,
                    firstname,
                    lastname,
                    email
                };
                
                localStorage.setItem("user", JSON.stringify(user));
            } else {
                alert(res.error);
            }
        }
    });
});

// *******************Pour afficher le format de password********************** */

function deleteOpinion(id_op) {
    if (confirm("Etes-vous sûr de vouloir supprimer votre opinion?")) {
        $.ajax({
            url: "user.php",
            type: "POST",
            dataType: "json",
            data: {
                option: "delete",
                id_op
            },
            success: () => {
                
                $("#tr_" + id_op).remove();
                window.location.replace("user.html");

            }
        });
    }
}

$(document).ready(function () {
    $("#password").focus(function () {
        $("span").css("display", "block");
    });
});

const id = user.id;

$.ajax({
    url: "user.php",
    type: "GET",
    dataType: "json",
    data: {
        option: "select_op_usr",
        user_id: id
    },
    success: (res) => {
        if (res.success) {
            res.opinions.forEach(component => {

                const row = $("<tr></tr>");
                row.attr("id_op", "tr_" + component.id_op)

                const title = $("<td></td>").text(component.title);
                const article_content = $("<td></td>").text(component.article_content);
                const advice = $("<td></td>").text(component.opinion_content);
                const author = $("<td></td>").text(component.name);

                const delctn = $("<td></td>");
                const delbtn = $("<button></button>");
                delbtn.html("<i class='fa fa-trash' aria-hidden='true'></i>");

                delctn.append(delbtn);

                delbtn.click(() => {
                    deleteOpinion(component.id_op);
                    });
                
                row.append(title, article_content, advice, author,delctn);

                $("#opinions").append(row);
                
            })            

        } else {
            alert(res.error);
        }
    }
});