<?php

require_once("../BDD/bac.php");

require("../function.php");

isConnected();

if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
else $method = $_GET;

switch ($method["option"]) {
    case "select_id":
        $req = $bac->prepare("SELECT firstname, lastname, email, admin FROM users WHERE id=?");
        $req->execute([$_SESSION["user_id"]]); 

        $user = $req->fetch(PDO::FETCH_ASSOC);

        echo json_encode(["success" => true, "user" => $user]);
        break;

    case "update":
        if (isset($_POST["firstname"], $_POST["lastname"]) && !empty(trim($_POST["firstname"])) && !empty(trim($_POST["lastname"]))) {
            $pwdsql = "";
            if (isset($_POST["password"]) && !empty(trim($_POST["password"]))) {
                $pwdsql = ",pwd = :pwd";

                $regex = "/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])[a-zA-Z0-9]{8,12}$/";
                if (!preg_match($regex, $_POST["password"])) {
                    echo json_encode(["success" => false, "error" => "Mot de passe au mauvais format"]);
                    die;
                }
                $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
            }

            $req = $bac->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname $pwdsql WHERE id = :id");
            $req->bindValue(":firstname", $_POST["firstname"]);
            $req->bindValue(":lastname", $_POST["lastname"]);
            if ($pwdsql != "") $req->bindValue(":pwd", $hash);
            $req->bindValue(":id", $_SESSION["user_id"]);
            $req->execute();

            echo json_encode(["success" => true]);
        } else {
            echo json_encode(["success" => false, "error" => "Erreur de mise a jour"]);
        }
        break;

        case "select_op_usr":

            if (isset($_GET["user_id"])) {
    
                $req = $bac->prepare("SELECT o.*, CONCAT(u.firstname, ' ', u.lastname) AS name, title, article_content
                FROM opinions o
                INNER JOIN users u ON u.id = o.user_id
                INNER JOIN articles a ON a.id_art = o.art_id
                WHERE (o.user_id=?) ORDER BY created_at DESC");
                
                $req->execute([$_GET["user_id"]]);
    
                $opinion = $req->fetchAll(PDO::FETCH_ASSOC);
        
                    echo json_encode(["success" => true, "opinions" => $opinion]);
                } else {
                    echo json_encode(["success" => false, "error" => "Erreur lors de la selection"]);
                }
        break;

        case 'delete':
            if (isset($_POST["id_op"]) && !empty(trim($_POST["id_op"]))) {
                $req = $bac->prepare("DELETE FROM opinions WHERE id_op=? AND user_id=?");
                $req->execute([$_POST["id_op"], $_SESSION["user_id"]]);
    
                echo json_encode(["success" => true]);
            } else {
                echo json_encode(["success" => false, "error" => "Erreur lors suppresssion"]);
            }
    
            break;

    default:
        echo json_encode(["success" => false, "error" => "Demande inconnue"]);
        break;
}

?>
